const { expect } = require("chai");
const { ethers, network } = require("hardhat");

describe("IDO System Tests", function () {
    let dexToken;
    let projectToken;
    let ido;

    let owner;
    let address1;
    let address2;
    let address3;

    // Means that you should send 10 dexTokens to get ability to win 100 projectTokens
    // You can free change prizeTokens
    // Carefully change min/max and ratio
    let prizeTokens = 100;
    let minAllocation = prizeTokens / 10;
    let maxAllocation = prizeTokens / 2;
    let ratio = 10;

    // Similar to deploy rules
    this.beforeEach(async function () {
        [owner, address1, address2, address3] = await hre.ethers.getSigners();

        const DexToken = await hre.ethers.getContractFactory("dexToken");
        const ProjectToken = await hre.ethers.getContractFactory("projectToken")
        const IDO = await hre.ethers.getContractFactory("IDO");

        dexToken = await DexToken.deploy();
        projectToken = await ProjectToken.deploy();
        ido = await IDO.deploy(dexToken.address, projectToken.address, prizeTokens, minAllocation, maxAllocation, ratio);

        await dexToken.deployed();
        await projectToken.deployed();
        await ido.deployed();

        // Start IDO instanly after constructing it
        await projectToken.mint(owner.address, prizeTokens);
        await projectToken.increaseAllowance(ido.address, prizeTokens);
        await ido.sendTokens();
    });

    describe("Basic functionality of IDO", function () {
        describe("View functions", function () {
            it("Should see min and max allocations", async function () {
                const min = await ido.minAllocation();
                const max = await ido.maxAllocation();
                expect(min).to.be.equal(minAllocation).to.be.equal(max / 5);
            });

            it("Should prizePool", async function () {
                const prize = await ido.numTokensReward();
                expect(prize).to.be.equal(prizeTokens);
            });

            it("Should participate 1 address", async function () {
                await dexToken.mint(address1.address, minAllocation / ratio);
                await dexToken.connect(address1).increaseAllowance(ido.address, minAllocation / ratio);

                await ido.connect(address1).joinIDO(minAllocation);

                const TVL = await ido.TVL();
                expect(TVL).to.be.equal(minAllocation / ratio);
            });

            it("Should participate 2 address", async function () {
                await dexToken.mint(address1.address, minAllocation / ratio);
                await dexToken.connect(address1).increaseAllowance(ido.address, minAllocation / ratio);
                await ido.connect(address1).joinIDO(minAllocation);

                await dexToken.mint(address2.address, minAllocation / ratio);
                await dexToken.connect(address2).increaseAllowance(ido.address, minAllocation / ratio);
                await ido.connect(address2).joinIDO(minAllocation);

                const TVL = await ido.TVL();
                expect(TVL).to.be.equal(2 * minAllocation / ratio);
            });
        });

        describe("View functions", function () {
            it("Should win 1 address", async function () {
                await dexToken.mint(address1.address, minAllocation / ratio);
                await dexToken.connect(address1).increaseAllowance(ido.address, minAllocation / ratio);
                await ido.connect(address1).joinIDO(minAllocation);

                await ido.end();
                await ido.connect(address1).getPrize();
                const prize = await projectToken.balanceOf(address1.address);
                expect(prize).to.be.equal(minAllocation);
            });

            it("Should win 2 address", async function () {
                await dexToken.mint(address1.address, minAllocation / ratio);
                await dexToken.connect(address1).increaseAllowance(ido.address, minAllocation / ratio);
                await ido.connect(address1).joinIDO(minAllocation);

                await dexToken.mint(address2.address, minAllocation / ratio);
                await dexToken.connect(address2).increaseAllowance(ido.address, minAllocation / ratio);
                await ido.connect(address2).joinIDO(minAllocation);

                await ido.end();
                await ido.connect(address1).getPrize();
                await ido.connect(address2).getPrize();
                const prize1 = await projectToken.balanceOf(address1.address);
                const prize2 = await projectToken.balanceOf(address2.address);
                expect(prize1).to.be.equal(prize2);
            });

            it("Should send dexTokens to owner", async function () {
                await dexToken.mint(address1.address, minAllocation / ratio);
                await dexToken.connect(address1).increaseAllowance(ido.address, minAllocation / ratio);
                await ido.connect(address1).joinIDO(minAllocation);

                await ido.end();
                await ido.getDexTokens();
                const balance = await dexToken.balanceOf(owner.address);
                expect(balance).to.be.equal(minAllocation / ratio);
            });

            it("Should return dexTokens cause address 3 loses", async function () {
                await dexToken.mint(address1.address, maxAllocation / ratio);
                await dexToken.connect(address1).increaseAllowance(ido.address, maxAllocation / ratio);
                await ido.connect(address1).joinIDO(maxAllocation);

                await dexToken.mint(address2.address, maxAllocation / ratio);
                await dexToken.connect(address2).increaseAllowance(ido.address, maxAllocation / ratio);
                await ido.connect(address2).joinIDO(maxAllocation);

                await dexToken.mint(address3.address, minAllocation / ratio);
                await dexToken.connect(address3).increaseAllowance(ido.address, minAllocation / ratio);
                await ido.connect(address3).joinIDO(minAllocation);

                await ido.end();
                await ido.connect(address3).getPrize();
                const balance = await dexToken.balanceOf(address3.address);
                expect(balance).to.be.equal(minAllocation / ratio);
            });
        });
    });

    describe("Failed basic functionality of IDO", function () {
        it("Should fail cause noone joined", async function () {
            await expect(
                ido.end()
            ).to.be.revertedWith("Anyone should participate to end");
        });

        it("Should fail cause ido hasn't ended yet", async function () {
            await expect(
                ido.getPrize()
            ).to.be.revertedWith("Ido hasn't ended yet");
        });

        it("Should fail cause try to send more than max", async function () {
            await dexToken.mint(address1.address, maxAllocation / ratio + 1);
            await dexToken.connect(address1).increaseAllowance(ido.address, maxAllocation / ratio + 1);

            await expect(
                ido.connect(address1).joinIDO(maxAllocation + 1)
            ).to.be.revertedWith("Check min and max allocations");
        });

        it("Should fail cause try to send less than min", async function () {
            await dexToken.mint(address1.address, minAllocation / ratio - 1);
            await dexToken.connect(address1).increaseAllowance(ido.address, minAllocation / ratio - 1);

            await expect(
                ido.connect(address1).joinIDO(minAllocation - 1)
            ).to.be.revertedWith("Check min and max allocations");
        });
    });
});