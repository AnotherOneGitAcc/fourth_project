// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.11;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract IDO is Ownable {
    IERC20 public projectToken;
    IERC20 public dexToken;

    uint256 public TVL;

    uint256 public numTokensReward;
    uint256 public minAllocation;
    uint256 public maxAllocation;
    uint256 public ratio;

    uint256 public winGroup;
    uint256 public groupCounter;

    bool isStarted = false;

    struct group {
        uint256 totalTokens;
        uint256 totalParticipants;
        bool isFilled;
    }

    mapping(uint256 => group) public groupsStructs;

    mapping(address => uint256) public groups;
    mapping(address => uint256) public participantTokens;

    constructor(
        address _tokenDex,
        address _tokenProject,
        uint256 _numTokensReward,
        uint256 _minAllocation,
        uint256 _maxAllocation,
        uint256 _ratio
    ) {
        dexToken = IERC20(_tokenDex);
        projectToken = IERC20(_tokenProject);

        numTokensReward = _numTokensReward;
        minAllocation = _minAllocation;
        maxAllocation = _maxAllocation;
        ratio = _ratio;
    }

    function sendTokens() public onlyOwner {
        projectToken.transferFrom(msg.sender, address(this), numTokensReward);

        isStarted = true;
    }

    function end() public onlyOwner {
        require(isStarted, "Admin should send tokens before start IDO");
        require(TVL != 0, "Anyone should participate to end");
        require(winGroup == 0, "Already ended");

        winGroup = random();
    }

    function random() internal returns (uint256) {
        if (TVL * ratio < numTokensReward) {
            winGroup = 1;
        } else {
            // Сделано чисто для вида (понятно, что так нельзя)
            winGroup = (block.timestamp % (groupCounter - 1)) + 1;
        }
        return winGroup;
    }

    function joinIDO(uint256 _tokensToGetAsPrize) public returns (uint256) {
        require(winGroup == 0, "IDO has ended");
        require(isStarted, "Admin should send tokens before start IDO");
        require(groups[msg.sender] == 0, "You've joined IDO already");

        uint256 tokensToPay = _tokensToGetAsPrize / ratio;

        require(
            dexToken.balanceOf(msg.sender) >= tokensToPay,
            "You should have this amount of tokens"
        );
        require(
            _tokensToGetAsPrize >= minAllocation &&
                _tokensToGetAsPrize <= maxAllocation,
            "Check min and max allocations"
        );

        dexToken.transferFrom(msg.sender, address(this), tokensToPay);

        if (groupCounter == 0) {
            groupCounter++;
            groupsStructs[groupCounter] = group(_tokensToGetAsPrize, 1, false);

            TVL += tokensToPay;
            groups[msg.sender] = groupCounter;
            participantTokens[msg.sender] = tokensToPay;

            return groupCounter;
        } else {
            if (
                groupsStructs[groupCounter].totalTokens + _tokensToGetAsPrize >=
                numTokensReward
            ) {
                groupsStructs[groupCounter].isFilled = true;

                groupCounter++;
                groupsStructs[groupCounter] = group(
                    _tokensToGetAsPrize,
                    1,
                    false
                );

                TVL += tokensToPay;
                groups[msg.sender] = groupCounter;
                participantTokens[msg.sender] = tokensToPay;

                return groupCounter;
            } else {
                groupsStructs[groupCounter].totalTokens += _tokensToGetAsPrize;
                groupsStructs[groupCounter].totalParticipants++;

                TVL += tokensToPay;
                groups[msg.sender] = groupCounter;
                participantTokens[msg.sender] = tokensToPay;

                return groupCounter;
            }
        }
    }

    function getPrize() public {
        require(winGroup != 0, "Ido hasn't ended yet");
        require(groups[msg.sender] != 0, "Needed to be joined in this IDO");

        if (groups[msg.sender] == winGroup) {
            projectToken.transfer(
                msg.sender,
                participantTokens[msg.sender] * ratio
            );
        } else {
            dexToken.transfer(msg.sender, participantTokens[msg.sender]);
        }
    }

    function getDexTokens() public onlyOwner {
        require(winGroup != 0, "Ido haven't ended yet");
        // Вернуть сдачу)
        projectToken.transfer(
            owner(),
            numTokensReward - groupsStructs[winGroup].totalTokens
        );
        dexToken.transfer(owner(), groupsStructs[winGroup].totalTokens / ratio);
    }
}
