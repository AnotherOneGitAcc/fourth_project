const hre = require("hardhat");

async function main() {

    const deployer = await hre.ethers.getSigners();

    const DexToken = await hre.ethers.getContractFactory("dexToken");
    const ProjectToken = await hre.ethers.getContractFactory("projectToken")
    const IDO = await hre.ethers.getContractFactory("IDO");

    const dexToken = await DexToken.deploy();
    const projectToken = await ProjectToken.deploy();
    const ido = await IDO.deploy(dexToken.address, projectToken.address, 100, 1, 50, 10);

    await dexToken.deployed();
    await projectToken.deployed();
    await ido.deployed();

    console.log("Dex token deployed to:", dexToken.address);
    console.log("Project token deployed to:", projectToken.address);
    console.log("IDO deployed to:", ido.address);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
