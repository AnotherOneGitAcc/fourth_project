// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.11;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract dexToken is ERC20 {
    address private _creator;

    constructor() ERC20("MyToken", "MTN") {
        _creator = msg.sender;
    }

    function mint(address to, uint256 amount) public {
        require((msg.sender == _creator), "Only creator can mint new tokens");

        _mint(to, amount);
    }
}
